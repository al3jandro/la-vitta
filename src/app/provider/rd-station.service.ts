import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RdStationService {
  config: any;
  constructor(public http: HttpClient) {
    this.config = {
      token: 'b07953f73d1e347d79627a73c6e45e1e',
      url: 'https://www.rdstation.com.br/api/1.3/conversions'
    };
  }
  /**
   *
   * @param items
   */
  public newsletter(items: any) {
    items = {
      token_rdstation: this.config.token,
      identificador: 'NewsLetter',
      Nome: items.nome,
      email: items.email,
    };
    return this.http.post(this.config.url, items, {
      headers: { 'Content-Type': 'application/json; charset=utf-8' }
    });
  }
  /**
   *
   * @param items
   */
  public contato(items: any) {
    items = {
      token_rdstation: this.config.token,
      identificador: 'Fale Conosco',
      Nome: items.nome,
      email: items.email,
      Telefone: items.telefone,
      Empresa: items.empresa,
      Mensagem: items.mensagem
    };
    return this.http.post(this.config.url, items, {
      headers: { 'Content-Type': 'application/json; charset=utf-8' }
    });
  }
  /**
   *
   * @param items
   * @param identificador
   */
  public agenda(items: any, identificador: string) {
    items = {
      token_rdstation: this.config.token,
      identificador: identificador,
      Nome: items.nome,
      email: items.email,
      Telefone: items.telefone,
      Empresa: items.empresa,
      Mensagem: items.mensagem
    };
    return this.http.post(this.config.url, items, {
      headers: { 'Content-Type': 'application/json; charset=utf-8' }
    });
  }
}
