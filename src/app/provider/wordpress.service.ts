import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class WordpressService {
  // URL del blog que vamos a trabajar con su REST API
  public URL = "https://vanguardadeveloper.com.br/lavitta/";
  public API = `${this.URL}wp-json/wp/v2/`;
  public items: any;

  constructor(private http: HttpClient) {}

  /**
   * Numero de post que quieres mostrar
   * @param id
   */
  getAll(id: number, filter: string = null) {
    if (filter === null) {
      this.items = this.http.get(`${this.API}posts?_embed&per_page=${id}`);
    } else {
      this.items = this.http.get(
        `${this.API}posts?_embed&per_page=${id}&${filter}`
      );
    }
    return this.items;
  }

  /**
   * Slug del post que vamos a mostrar
   * @param id
   */
  getOne(id: string, filter: string = null) {
    if (filter === null) {
      this.items = this.http.get(`${this.API}posts?_embed&slug=${id}`);
    } else {
      this.items = this.http.get(
        `${this.API}posts?_embed&slug=${id}&${filter}`
      );
    }
    return this.items;
  }

  /**
   * Search Posts
   * @param id
   */
  getSearch(id: string, category: number) {
    if (id != null) {
      this.items = this.http.get(`${this.API}posts?_embed&search=${id}`);
    }
    return this.items;
  }
}
