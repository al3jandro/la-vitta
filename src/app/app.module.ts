import { WordpressService } from './provider/wordpress.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoutingModule } from './module/routing/routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { NavComponent } from './widget/nav/nav.component';
import { HeaderComponent } from './widget/header/header.component';
import { TratamentosComponent } from './widget/tratamentos/tratamentos.component';
import { BlockComponent } from './widget/block/block.component';
import { TimeComponent } from './widget/time/time.component';
import { ParallaxComponent } from './widget/parallax/parallax.component';
import { TestimonialComponent } from './widget/testimonial/testimonial.component';
import { MapsComponent } from './widget/maps/maps.component';
import { ContatoComponent } from './widget/contato/contato.component';
import { FooterComponent } from './widget/footer/footer.component';
import { HomeComponent } from './layout/home/home.component';
import { ClinicaComponent } from './layout/clinica/clinica.component';
import { TratamentoComponent } from './layout/tratamento/tratamento.component';
import { SucessoComponent } from './layout/sucesso/sucesso.component';
import { ContatosComponent } from './layout/contatos/contatos.component';
import { BlogComponent } from './layout/blog/blog.component';
import { StoryComponent } from './layout/blog/story/story.component';
import { VideoComponent } from './widget/video/video.component';
import { NewsComponent } from './widget/news/news.component';
import { TruncatePipe } from './pipe/truncate.pipe';
import { InseminacaoComponent } from './layout/tratamento/inseminacao/inseminacao.component';
import { FertilizacaoComponent } from './layout/tratamento/fertilizacao/fertilizacao.component';
import { CongelamentoComponent } from './layout/tratamento/congelamento/congelamento.component';
import { GeneticoComponent } from './layout/tratamento/genetico/genetico.component';
import { ScrollDirective } from './directive/scroll.directive';
import { SingleComponent } from './layout/sucesso/single/single.component';
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HeaderComponent,
    TratamentosComponent,
    BlockComponent,
    TimeComponent,
    ParallaxComponent,
    TestimonialComponent,
    MapsComponent,
    ContatoComponent,
    FooterComponent,
    HomeComponent,
    ClinicaComponent,
    TratamentoComponent,
    SucessoComponent,
    ContatosComponent,
    BlogComponent,
    StoryComponent,
    VideoComponent,
    NewsComponent,
    TruncatePipe,
    InseminacaoComponent,
    FertilizacaoComponent,
    CongelamentoComponent,
    GeneticoComponent,
    ScrollDirective,
    SingleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAHAW2QhYaIfY68THlu-wn4sBcTvM0KGII'
    }),
    FormsModule,
    ReactiveFormsModule,
    RoutingModule,
    HttpClientModule,
    NgxSpinnerModule,
    NgxPaginationModule
  ],
  providers: [WordpressService],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
