import { StoryComponent } from './../../layout/blog/story/story.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from '../../layout/home/home.component';
import { ClinicaComponent } from '../../layout/clinica/clinica.component';
import { SingleComponent } from '../../layout/sucesso/single/single.component';
import { SucessoComponent } from '../../layout/sucesso/sucesso.component';
import { BlogComponent } from './../../layout/blog/blog.component';
import { ContatosComponent } from '../../layout/contatos/contatos.component';
import { InseminacaoComponent } from '../../layout/tratamento/inseminacao/inseminacao.component';
import { FertilizacaoComponent } from '../../layout/tratamento/fertilizacao/fertilizacao.component';
import { CongelamentoComponent } from './../../layout/tratamento/congelamento/congelamento.component';
import { GeneticoComponent } from './../../layout/tratamento/genetico/genetico.component';


const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    pathMatch: "full"
  },
  {
    path: "a-clinica",
    component: ClinicaComponent
  },
  {
    path: "tratamentos/inseminacao",
    component: InseminacaoComponent
  },
  {
    path: "tratamentos/fertilizacao",
    component: FertilizacaoComponent
  },
  {
    path: "tratamentos/congelamento",
    component: CongelamentoComponent
  },
  {
    path: "tratamentos/genetico",
    component: GeneticoComponent
  },
  {
    path: "sucessos",
    component: SucessoComponent
  },
  {
    path: "sucessos/:id",
    component: SingleComponent
  },
  {
    path: "blog",
    component: BlogComponent
  },
  {
    path: "blog/:id",
    component: StoryComponent
  },
  {
    path: "contato",
    component: ContatosComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})

export class RoutingModule { }
