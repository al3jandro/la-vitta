import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { WordpressService } from './../../provider/wordpress.service';


@Component({
  selector: 'app-sucesso',
  templateUrl: './sucesso.component.html',
  styleUrls: ['./sucesso.component.scss']
})
export class SucessoComponent implements OnInit {

  feeds: any;
  p: number;

  constructor(
    private spinner: NgxSpinnerService,
    private wordpress: WordpressService
  ) { }

  ngOnInit() {
    this.p = 1;
    this.getPosts();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 3000);
  }

  private getPosts() {
    this.wordpress.getAll(100, 'categories=3').subscribe(data => {
      this.feeds = data;
      console.log(data);
    });
  }
}
