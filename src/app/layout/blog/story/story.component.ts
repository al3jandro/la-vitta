import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { WordpressService } from './../../../provider/wordpress.service';
import { Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {
  id: string;
  feed: any;
  url: any = 'https://lavittaamazonas.com.br/';

  constructor(
    private wordpress: WordpressService,
    private meta: Meta,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {
    this.route.url.subscribe(u => {
      this.id = this.route.snapshot.params.id;
    });
  }

  ngOnInit() {
    this.spinner.show();
    this.single();
  }

  private single() {
    this.wordpress.getOne(this.id).subscribe(data => {
      this.feed = data;
      this.tags(data);
      this.spinner.hide();
      console.log(data[0].slug);
    });
  }

  private tags(data) {
    this.meta.updateTag({ name: 'twitter:card', content: 'summary' });
    this.meta.updateTag({ name: 'twitter:site', content: 'La Vitta' });
    this.meta.updateTag({ name: 'twitter:title', content: data[0].title.rendered });
    this.meta.updateTag({ name: 'twitter:description', content: data[0].excerpt.rendered });
    this.meta.updateTag({
      name: 'twitter:image',
      content: data[0]._embedded['wp:featuredmedia'][0].link
    });
    this.meta.updateTag({ property: 'og:type', content: 'article' });
    this.meta.updateTag({ property: 'og:site_name', content: 'La Vitta' });
    this.meta.updateTag({ property: 'og:title', content: data[0].title.rendered });
    this.meta.updateTag({ property: 'og:description', content: data[0].excerpt.rendered });
    this.meta.updateTag({ property: 'og:image', content: data[0]._embedded['wp:featuredmedia'][0].link });
    this.meta.updateTag({ property: 'og:url', content: `${this.url}/blog/${data[0].slug}` });
  }
}
