import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WordpressService } from './../../provider/wordpress.service';


@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  p: number;
  flag: boolean;
  flag2: boolean;
  feeds: any;
  countSearch: number;
  item: string;

  constructor(
    private wordpress: WordpressService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.p = 1;
    this.getPosts();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 3000);
  }

  voltar() {
    this.getPosts();
    this.flag = true;
    this.item = '';
  }

  private getPosts() {
    this.flag = true;
    this.countSearch = 0;
    this.wordpress.getAll(100, 'categories=2').subscribe(data => {
      this.feeds = data;
      console.log(data);
    });
  }

  search() {
    this.wordpress
      .getAll(100, 'categories=2&search=' + this.item)
      .subscribe(data => {
        this.countSearch = data.length;
        if (this.countSearch > 0) {
          this.flag = true;
          this.feeds = data;
          console.log('flag: ', this.flag);
          console.log(data);
          console.log('Cant Post', this.countSearch);
        } else {
          this.flag = false;
          this.countSearch = 0;
          console.log(data);
          console.log('Cant Post', this.countSearch);
        }
      });
  }
}
