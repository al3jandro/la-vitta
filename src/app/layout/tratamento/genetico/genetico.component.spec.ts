import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneticoComponent } from './genetico.component';

describe('GeneticoComponent', () => {
  let component: GeneticoComponent;
  let fixture: ComponentFixture<GeneticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
