import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FertilizacaoComponent } from './fertilizacao.component';

describe('FertilizacaoComponent', () => {
  let component: FertilizacaoComponent;
  let fixture: ComponentFixture<FertilizacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FertilizacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FertilizacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
