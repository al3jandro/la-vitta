import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InseminacaoComponent } from './inseminacao.component';

describe('InseminacaoComponent', () => {
  let component: InseminacaoComponent;
  let fixture: ComponentFixture<InseminacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InseminacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InseminacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
