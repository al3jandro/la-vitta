import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongelamentoComponent } from './congelamento.component';

describe('CongelamentoComponent', () => {
  let component: CongelamentoComponent;
  let fixture: ComponentFixture<CongelamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongelamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongelamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
