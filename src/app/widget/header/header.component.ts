import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  X: any;
  Y: any;
  W: any;
  Z: any;
  A: any;
  B: any;

  constructor() {}

  ngOnInit() {}

  test(event) {
    this.X = event.clientX / 14;
    this.Y = event.clientY / 13;
    this.W = event.clientX / 17;
    this.Z = event.clientY / 18;
    this.A = event.clientX;
    this.B = event.clientY;
  }

  testUp(event) {
    this.X = 0;
    this.Y = 0;
    this.W = 0;
    this.Z = 0;
  }
}
