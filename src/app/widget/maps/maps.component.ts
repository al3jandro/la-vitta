import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {
  public map: any = { lat: -3.1291283, lng: -60.0246696 };

  constructor() {}

  ngOnInit() {}
}
