import { Component, OnInit } from '@angular/core';
import { WordpressService } from './../../provider/wordpress.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  items: any;
  constructor(private wordpress: WordpressService) { }

  ngOnInit() {
    this.getPosts();
  }

  private getPosts() {
    this.wordpress.getAll(3, 'categories=2').subscribe(
      data => {
        this.items = data;
        console.log(data);
      }
    );
  }

}
