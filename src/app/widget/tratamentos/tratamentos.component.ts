import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-tratamentos",
  templateUrl: "./tratamentos.component.html",
  styleUrls: ["./tratamentos.component.scss"]
})
export class TratamentosComponent implements OnInit {
  imgSrc1 = "./assets/imgs/tratamentos/inseminacion.png";
  imgSrc2 = "./assets/imgs/tratamentos/fiv.png";
  imgSrc3 = "./assets/imgs/tratamentos/congelamento.png";
  imgSrc4 = "./assets/imgs/tratamentos/genetico.png";

  constructor() {}

  ngOnInit() {}

  over1(e) {
    this.imgSrc1 = "./assets/imgs/tratamentos/inseminacion_hover.png";
  }
  up1(e) {
    this.imgSrc1 = "./assets/imgs/tratamentos/inseminacion.png";
  }

  over2(e) {
    this.imgSrc2 = "./assets/imgs/tratamentos/fiv_hover.png";
  }
  up2(e) {
    this.imgSrc2 = "./assets/imgs/tratamentos/fiv.png";
  }

  over3(e) {
    this.imgSrc3 = "./assets/imgs/tratamentos/congelamento_hover.png";
  }
  up3(e) {
    this.imgSrc3 = "./assets/imgs/tratamentos/congelamento.png";
  }

  over4(e) {
    this.imgSrc4 = "./assets/imgs/tratamentos/genetico_hover.png";
  }
  up4(e) {
    this.imgSrc4 = "./assets/imgs/tratamentos/genetico.png";
  }
}
