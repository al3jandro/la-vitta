import { Component, OnInit } from '@angular/core';
import { RdStationService } from '../../provider/rd-station.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert';


@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.scss']
})
export class ContatoComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(
    private rdStation: RdStationService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nome: ['', Validators.required],
      telefone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      empresa: ['', Validators.required],
      mensagem: ['', Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }


  agendar() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.rdStation
      .agenda(this.registerForm.value, 'Contato')
      .subscribe(
        () => {
          swal('Obrigado',
            'Em breve, um consultor entrará em contato com você',
            'success');
        },
        error => {
          console.log('Error', error);
        }
      );
  }
}
